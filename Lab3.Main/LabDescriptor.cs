﻿using System;

using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region P1

        public static Type I1 = typeof(IJakaKawa);
        public static Type I2 = typeof(IJakSlodko);
        public static Type I3 = typeof(IZasilanie);

        public static Type Component = typeof(KontrolerKawy);

        public delegate object GetInstance(object component);

        public static GetInstance GetInstanceOfI1 = (Component) => Component;
        public static GetInstance GetInstanceOfI2 = (Component) => Component;
        public static GetInstance GetInstanceOfI3 = (Component) => Component;

        #endregion

        #region P2

        public static Type Mixin = typeof(ZasilanieMixin);
        public static Type MixinFor = typeof(IZasilanie);

        #endregion
    }
}
