﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lab3.Contract;

namespace Lab3.Implementation
{
    internal class Mleko : IDodatek
    {
        public void Dodaj()
        {
            Console.WriteLine("Dodano!");
        }

        public void NieDodaj()
        {
            Console.WriteLine("NIE!");
        }
    }
}
