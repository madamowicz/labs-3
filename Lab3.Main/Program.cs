﻿using System;

using Lab3.Implementation;
using Lab3.Contract;

namespace Lab3
{
    public static class ZasilanieMixin
    {
        public static void DostarczPrad(this IZasilanie target)
        {
            target.DostarczPrad();
        }
    }


    class Program
    {
        static void Main(string[] args)
        {

            var kontroler = new KontrolerKawy();
            kontroler.Operacje();
            kontroler.DostarczPrad();

        }
    }
}
