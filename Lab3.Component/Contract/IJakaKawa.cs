﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lab3.Implementation;

namespace Lab3.Contract
{
    public interface IJakaKawa
    {
        void DostepneKawy();
        string WybierzKawe();
        void WybranaKawa();
    }
}
