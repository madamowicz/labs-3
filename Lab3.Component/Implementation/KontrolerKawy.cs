﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    public class KontrolerKawy : IZasilanie, IJakaKawa, IJakSlodko
    {
        //Izasilanie
        public void DostarczPrad()
        {
            Console.WriteLine("Zasilanie...");
        }
        public void DostarczonyPrad()
        {
            Console.WriteLine("Jest moc!");
        }
        //IJakaKawa
        public void DostepneKawy()
        {
            Console.WriteLine("1,2,3,4");
        }
        public string WybierzKawe()
        {
            return "cappuccino";
        }
        public void WybranaKawa()
        {
            Console.WriteLine("Wybrano Cappuccino");
        }

        //IJakSlodko
        public void DostepnyPoziom()
        {
            Console.WriteLine("1,2,3");
        }
        public string WybierzPoziom()
        {
            return "bez cukru";
        }
        public void WybranyPoziom()
        {
            Console.WriteLine("Wybrano poziom 1");
        }

        //Operacje
        public void Operacje()
        {
            IZasilanie prad = new KontrolerKawy();
            prad.DostarczPrad();
            prad.DostarczonyPrad();

            IJakaKawa kawa = new KontrolerKawy();
            kawa.DostepneKawy();
            kawa.WybierzKawe();
            kawa.WybranaKawa();

            IJakSlodko cukier = new KontrolerKawy();
            cukier.DostepnyPoziom();
            cukier.WybierzPoziom();
            cukier.WybranyPoziom();

        }
    }
}
