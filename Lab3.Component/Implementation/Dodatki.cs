﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;


namespace Lab3.Implementation
{
    class Dodatki
    {
        private IDodatek slodzik;
        private IDodatek mleko;

        public void Dostarcz()
        {
            slodzik.Dodaj();
            mleko.Dodaj();
        }
        public void NieDostarcz()
        {
            slodzik.NieDodaj();
            mleko.NieDodaj();
        }
    }
}
